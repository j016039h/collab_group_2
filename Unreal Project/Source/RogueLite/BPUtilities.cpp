// Fill out your copyright notice in the Description page of Project Settings.


#include "BPUtilities.h"

TArray<UClass*> UBPUtilities::GetClasses(UClass* ParentClass)
{
	TArray<UClass*> ChildClasses;

	const FString ParentClassName = ParentClass->GetName();
	UObject* ClassPackage = ANY_PACKAGE;
	UClass* ParentBPClass = FindObject<UClass>(ClassPackage, *ParentClassName);

	for (TObjectIterator<UClass> it; it; ++it)
	{
		if (it->IsChildOf(ParentBPClass))
		{
			if (it->GetName() != ParentClassName)
			{
				ChildClasses.Add(*it);
			}
		}
	}

	return ChildClasses;
}