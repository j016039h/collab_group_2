// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BPUtilities.generated.h"

/**
 * 
 */
UCLASS()
class ROGUELITE_API UBPUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

		UFUNCTION(BlueprintCallable, Category = "BP Utilities", meta = (DisplayName = "Get All Child Classes", Keywords = "Get All Child Classes"))
		static TArray<UClass*> GetClasses(UClass* ParentClass);
};
